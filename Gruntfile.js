// Grunt configuratie voor DUO UX Library
// Alle grunt taken staan in de /tasks map
// Eventuele configuratie staat in /tasks/options

module.exports = (grunt) => {
  'use strict';

  // Laad alle plugins automatisch in
  require('load-grunt-tasks')(grunt);

  var _ = require('lodash');

  var config = {
    pkg: grunt.file.readJSON('package.json'),
    env: process.env
  };

  // Laad alle configuratie files in de tasks/options en tasks map
  _.extend(config, loadConfig('./tasks/options/'));
  grunt.initConfig(config);
  grunt.task.loadTasks('tasks/');
};

function loadConfig(path) {
  var glob = require('glob');
  var object = {};
  var key;

  glob.sync('*', {cwd: path}).forEach(function(option) {
    key = option.replace(/\.js$/,'');
    object[key] = require(path + option);
  });

  return object;
}
