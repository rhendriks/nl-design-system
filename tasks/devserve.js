module.exports = (grunt) => {
    'use strict';

    grunt.registerTask('devserve', 'Compile and start a webserver', [
        'clean:docs',
        'clean:dist',
        'copy:dist',
        'typescript:dist',
        'sass:dist',
        'postcss:dist', // Build css
        'concat:dist',
        'concat:tridion',
        'usebanner:dist',
        'concurrent:devwatcher'
    ]);
};