module.exports = {
  dist: ['dist/**'],
  docs: ['docs/**'],
  uno: ['_docs/themes/duo-ux/source/uno/**'],
  theme: ['src/theme/**']
};
