module.exports = {
  // Launches a concurrent watcher so any change will trigger regeneration
  // of docs
  watcher: {
    tasks: [
      'watch:styles',
      'watch:typescript',
      'typescript:watch',
      'watch:assets',
      'docs:serve'
    ],
    options: {
      logConcurrentOutput: true,
			spawn: false,
      limit: 20
    }
  },
    devwatcher: {
        tasks: [
            'watch:devstyles',
            'watch:devtypescript',
            'typescript:dist',
            'watch:devassets',
        ],
        options: {
            logConcurrentOutput: true,
            limit: 20
        }
    }
};
