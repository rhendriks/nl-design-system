module.exports = {
    options: {
      changelogOpts: {
        // conventional-changelog options go here
        preset: 'angular'
      }
    },
  generate: {
    src: 'HISTORY.MD'
  }
};
