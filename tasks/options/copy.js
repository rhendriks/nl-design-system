var grunt = require('grunt');
var theme = grunt.option('theme') || 'duo';
var themeCwd = 'themes/' + theme;

module.exports = {
  // Copies all the asset from the src folder to the dist folder
  dist: {
    files: [
      {
        expand: true,
        cwd: 'src',
        src: ['**', '!**/*.scss', '!**/*.ts', '!settings/icon-template.css'],
        dest: 'dist/'
      }
    ]
  },
  // Copies all the assets from the theme folder to the src folder
  theme: {
    files: [
      {
        expand: true,
        cwd: themeCwd,
        src: '**',
        dest: 'src/theme'
      }
    ]
  },
  // Copies all the asset from the dist folder to the uno folder
  uno: {
    files: [
      {
        expand: true,
        cwd: 'dist/',
        src: '**',
        dest: '_docs/themes/duo-ux/source/uno/'
      },
      {
        /**
         * Copy systemjs, for dist,
         * systemjs is installed as node_module dependency
         **/
        expand: true,
        cwd: 'node_modules/systemjs/dist/',
        src: ['system.js'],
        dest: '_docs/themes/duo-ux/source/uno/vendor/js/'
      },
      {
        /**
         * Copy when, for dist,
         * when is installed as node_module dependency
         **/
        expand: true,
        cwd: 'node_modules/when/es6-shim/',
        src: ['Promise.min.js'],
        dest: '_docs/themes/duo-ux/source/uno/vendor/js/'
      }
    ]
  },
  // Copies all the asset from the src folder to the docs folder
  dev: {
    files: [
      {
        expand: true,
        cwd: 'src',
        src: ['**', '!**/*.scss', '!**/*.ts', '!settings/icon-template.css'],
        dest: '_docs/themes/duo-ux/source/uno/'
      },
      {
        /**
         * Copy systemjs, for dist,
         * systemjs is installed as node_module dependency
         **/
        expand: true,
        cwd: 'node_modules/systemjs/dist/',
        src: ['system.js'],
        dest: '_docs/themes/duo-ux/source/uno/vendor/js/'
      },
      {
        /**
         * Copy when, for dist,
         * when is installed as node_module dependency
         **/
        expand: true,
        cwd: 'node_modules/when/es6-shim/',
        src: ['Promise.min.js'],
        dest: '_docs/themes/duo-ux/source/uno/vendor/js/'
      }
    ]
  },

  // Copy changelog to docs
  changelog: {
    files: [
      {
        expand: false,
        cwd: '',
        src: ['HISTORY.MD'],
        dest: '_docs/source/changelog.md'
      },
    ]
  }
};
