module.exports = {
  unit: {
    configFile: './config/karma.conf.js',
    browsers: ['ChromeHeadless'],
    singleRun: true
  },

  test: { // alias for unit
    configFile: './config/karma.conf.js',
    singleRun: true
  },

  watch: {
    configFile: './config/karma.conf.js',
    detectBrowsers: {
      enabled: false
    },
    browsers: [
      'ChromeHeadless'
    ],
    singleRun: false
  }
};