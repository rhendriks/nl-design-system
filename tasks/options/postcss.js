const scss = require('postcss-scss');

const settings = {
  browsers: [
    '> 1% in my stats',
    'last 2 versions',
    'last 10 Firefox versions',
    'last 10 Chrome versions'
  ],

  stats: 'config/browserlist-stats.json'
};

module.exports = {
  dev: {
    options: {
      map: true, // inline sourcemaps

      processors: [
        require('pixrem')({rootValue: '10px'}), // add fallbacks for rem units
        require('autoprefixer')(settings) // add vendor prefixes
      ]
    },
    // Output to _docs folder instead of docs, so Hexo's browsersync will pick up changes
    src: '_docs/themes/duo-ux/source/uno/**/*.css'
  },

  dist: {
    options: {
      map: true, // inline sourcemaps

      processors: [
        require('pixrem')({rootValue: '10px'}), // add fallbacks for rem units
        require('autoprefixer')(settings), // add vendor prefixes
        require('cssnano')({discardUnused: false, reduceIdents: false, normalizeUrl: false}) // minify the result
      ]
    },
    src: 'dist/**/*.css'

  },

  lint: {
    options: {
      map: false, // inline sourcemaps,
      failOnError: true,
      syntax: scss,
      writeDest: false, // don't write, just lint
      processors: [
        require('stylelint')({configFile:'./.stylelintrc'}) // check for code style
      ]
    },
    src: 'src/**/*.scss'
  }
};
