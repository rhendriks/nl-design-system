module.exports = {

  dist: {
    files: {
      src: ['dist/**/*.js', 'dist/**/*.css']
    },
    options: {
      position: 'top',
      banner: '/*!\n' +
        'NL Design System Componenten library <%= pkg.version %>, build date <%= grunt.template.today("dd-mm-yyyy") %>\n' +
        'Copyright 2011-2020 The NL Design System Authors\n' +
        'Copyright 2011-2020 Duo\n' +
        'Author: DUO & The NL Design System Authors\n' +
        'Author URI: <%= pkg.author.website %>\n' +
        'License: EUPL v1.2\n' +
        'License URL: https://joinup.ec.europa.eu/software/page/eupl5\n' +
        'Version: <%= pkg.version %>\n' +
        '*/',
      linebreak: true
    }
  },
  changelog: {
    files: {
      src: '_docs/source/changelog.md'
    },

    options: {
      position: 'top',
      banner: `---
layout: content-page
permalink: changelog/
title: Changelog
---`,
      linebreak: true
    }
  }
};
