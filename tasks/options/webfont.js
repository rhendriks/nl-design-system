module.exports = {
    generate: {
        src: 'src/images/icons/*.svg',
        dest: 'src/core/fonts',
        destCss: 'src/core/generic',
        options: {
            engine: 'node',
            font: 'duo-basic',
            fontFilename: 'duo-basic',
            hashes: true,
            htmlDemo: false,
            normalize:  true,
            optimize: false,
            order: 'eot, ttf, woff, svg',
            styles: 'font',
            stylesheet: 'scss',
            template: 'src/settings/icon-template.css',
            templateOptions: {
                baseClass: 'icon',
                classPrefix: 'icon--'
            },
            types: 'eot, ttf, woff, svg'
        }
    }
};