module.exports = (grunt) => {
  'use strict';

  grunt.registerTask('serve', 'Compile and start a webserver', [
    'sass:dev',
    'postcss:dev',
    'copy:dev',
    'typescript:dev',
    'docs:generate', // Generate docs, sass and assets once, then launch watcher
    'concurrent:watcher'
  ]);
};
