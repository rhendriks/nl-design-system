import {FilterList} from '../../../src/components/list/filterlist';
import {} from 'jasmine';
import Spy = jasmine.Spy;
import createSpy = jasmine.createSpy;

describe('FilterList component', () => {
    const data: Array<any> = [
        {
            "value": "Voorbereiden",
            "id": "filter-voorbereiden"
        },
        {
            "value": "Bekostiging bijzonder",
            "id": "filter-bekostiging-bijzonder"
        },
        {
            "value": "Studiefinanciering",
            "id": "filter-studiefinanciering"
        },
        {
            "value": "Basisbeurs",
            "id": "filter-Basisbeurs"
        },
        {
            "value": "Lesgeld",
            "id": "filter-lesgeld"
        },
    ];

    let element:HTMLElement,
        filterList:FilterList;

    beforeEach(() => {
        element = document.createElement('ul');
        document.body.appendChild(element);
        jasmine.clock().install();
    });

    it('should delete the filter element when the delete function is called', ()  => {

        filterList = new FilterList(element, data);
        const el = document.getElementById('filter-lesgeld');


        // Execute
        filterList.delete(el);

        // Test
        expect(document).not.toContain(el);
    });

    it('should delete all filter elements when the deleteAll function is called', ()  => {

        filterList = new FilterList(element, data);

        // Execute
        filterList.deleteAll();

        // Test
        expect(element.innerHTML).toBe('');
    });

    it('should delete all filter elements when the destroy function is called', ()  => {

        filterList = new FilterList(element, data);

        // Execute
        filterList.destroy();

        // Test
        expect(element.innerHTML).toBe('');
    });

    describe('events', () => {

        it('should fire a `filterlist-deleted-item` event when an filter element is deleted', () => {
            const spy: any = createSpy();
            filterList = new FilterList(element, data);
            const el = document.getElementById('filter-lesgeld');
            // noinspection TsLint
            element.addEventListener('filterlist-deleted-item', spy);
            filterList.delete(el);

            // execute
            jasmine.clock().tick(1);
            expect(spy).toHaveBeenCalled();
        });

        it('should fire a `filterlist-deleted` event when all filter items are deleted', () => {
            const spy: any = createSpy();
            filterList = new FilterList(element, data);
            // noinspection TsLint
            element.addEventListener('filterlist-deleted', spy);
            filterList.deleteAll();

            // execute
            jasmine.clock().tick(1);
            expect(spy).toHaveBeenCalled();
        });
    });

    afterEach(() => {
        jasmine.clock().uninstall();
        element.parentElement.removeChild(element);
    });
});
