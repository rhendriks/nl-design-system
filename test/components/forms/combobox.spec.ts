import {Combobox, MODE_AUTOCOMPLETE} from '../../../src/components/form/combobox';
import {Events} from '../../../testutils/Events';
import {Utils} from '../../../src/core/utils';
import {KeyboardEvents, KEYCODE_DOWN, KEYCODE_ENTER, KEYCODE_ESC, KEYCODE_UP} from '../../../testutils/KeyboardEvents';
import Spy = jasmine.Spy;
import createSpy = jasmine.createSpy;

describe('Combobox component', () => {

    let element: HTMLElement;
    let cb: Combobox;

    const TPL: string = `<div>
                        <input type="text" class="combobox__input" />
                        <button type="button" class="combobox__toggle"></button>
                        <div class="combobox__autocomplete">
                             <div class="combobox__list-wrapper">
                                 <ul class="combobox__list" tabindex="0" hidden>
                                 </ul>
                             </div>
                         </div>
                    </div>`;

    const DATA: Array<any> = [
        {
            label: 'Rood',
            value: 'ff0000'
        },
        {
            label: 'Groen',
            value: '00ff00'
        },
        {
            label: 'Blauw',
            value: '0000ff'
        }
    ];

    const DATA_STRINGS: Array<string> = [
        'rood',
        'groen',
        'blauw',
        'paars',
        'roze'
    ];

    function getElement(tpl: string): HTMLElement {
        element.innerHTML = tpl;
        return element;
    }

    beforeEach(() => {
        // noinspection TsLint
        if (window.top['callPhantom']) {
            // PhantomJS does not support original implementation
            spyOn(Utils, 'CreateNode').and.callFake((html: string) => new DOMParser().parseFromString(html, 'text/xml').firstChild as HTMLElement);
        }
    });

    beforeEach(() => {
        element = document.createElement('div');
        document.body.appendChild(element);
        jasmine.clock().install();
    });

    describe('setup', () => {

        it('should throw an error if no or an invalid host is specified', () => {

            let thrown: string;

            // Execute
            try {
                cb = new Combobox(null);
            } catch (e) {
                thrown = e.message;
            }

            // Test
            expect(thrown).toBe('No host element specified');
        });

        it('should check if an input is present', () => {
            const el: Element = document.createElement('div');
            let thrown: string = '';

            // Execute
            try {
                cb = new Combobox(el);
            } catch (e) {
                thrown = e.message;
            }

            // Test
            expect(thrown).toBe('Host element should contain a text input');

        });

        it('should add the necessary classes', () => {
            // Setup
            const el: Element = document.createElement('div');
            el.innerHTML = `<input class="combobox__input" />`;

            // Execute
            cb = new Combobox(el);

            // Test
            expect(el.classList.contains('combobox')).toBe(true, 'El should contain class combobox');
        });

        it('should add the icon element if not present', () => {
            // Setup
            const el: Element = getElement(`<div><input class="combobox__input" /></div>`);

            // Execute
            cb = new Combobox(el);

            // Test
            expect(el.querySelector('.combobox__icon')).not.toBeNull();
        });

        it('should add the button element if not present', () => {
            // Setup
            const el: Element = getElement(`<div><input class="combobox__input" /></div>`);

            // Execute
            cb = new Combobox(el);

            // Test
            expect(el.querySelector('.combobox__toggle')).not.toBeNull();
        });

        it('should add the autocomplete container element if not present', () => {
            // Setup
            const el: Element = getElement(`<div><input class="combobox__input" /></div>`);

            // Execute
            cb = new Combobox(el);

            // Test
            expect(el.querySelector('.combobox__list')).not.toBeNull();
        });

        it('should not add a second icon if the host already contains an icon', () => {
            // Setup
            const el: Element = getElement(
                `<div>
                        <input class="combobox__input" />
                        <i class="combobox__icon icon icon-magnifier" role="presentation"></i>
                    </div>`
            );

            // Execute
            cb = new Combobox(el);

            // Test
            expect(el.querySelectorAll('.combobox__icon').length).toBe(1);
        });

        it('should not add a second button if the host already contains an icon', () => {
            // Setup
            const el: Element = getElement(
                `<div>
                        <input class="combobox__input" />
                        <button type="button" class="combobox__toggle"></button>
                    </div>`
            );

            // Execute
            cb = new Combobox(el);

            // Test
            expect(el.querySelectorAll('.combobox__toggle').length).toBe(1);
        });

        it('should not add a second autocomplete container if the host already contains an icon', () => {
            // Setup
            const el: Element = getElement(
                `<div>
                        <input class="combobox__input" />
                        <button type="button" class="combobox__toggle"></button>
                        <div class="combobox__autocomplete">
                             <div class="combobox__list-wrapper">
                                 <ul class="combobox__list" tabindex="0" hidden>
                                 </ul>
                             </div>
                         </div>
                    </div>`
            );

            // Execute
            cb = new Combobox(el);

            // Test
            expect(el.querySelectorAll('.combobox__list').length).toBe(1);
        });
    });

    describe('usage', () => {

        it('should return the value of the input', () => {
            // Setup
            const el: HTMLElement = getElement(TPL);
            cb = new Combobox(el);

            // Execute
            el.querySelector('input').value = 'TEST VALUE';

            // Test
            expect(cb.query).toEqual('TEST VALUE');
        });

        it('should show the loading icon', () => {
            const el: HTMLElement = getElement(TPL);
            cb = new Combobox(el);

            // Execute
            cb.loading = true;

            // Test
            expect(el.querySelector('.combobox__icon--loading')).not.toBeNull();
            expect(cb.loading).toBeTruthy();
        });

        it('should hide the loading icon', () => {
            const el: HTMLElement = getElement(TPL);
            cb = new Combobox(el);

            // Execute
            cb.loading = false;

            // Test
            expect(el.querySelector('.combobox__icon--loading')).toBeNull();
            expect(cb.loading).toBeFalsy();
        });

        it('should not attempt to filter if no data is provided', () => {
            const el: HTMLElement = getElement(TPL);
            cb = new Combobox(el);

            let spy: Spy = spyOn(cb, 'filterFunction');
            // Execute
            KeyboardEvents.KeyUp(el.querySelector('input'), 'r'.charCodeAt(0));

            // Test
            expect(spy).not.toHaveBeenCalled();
        });

        it('should open the dropdown via the open() method', () => {
            const el: HTMLElement = getElement(TPL);
            cb = new Combobox(el);
            cb.data = DATA;

            // Execute
            cb.open();

            // Test
            expect(cb.isOpen).toBeTruthy();
            expect(el.querySelector('.combobox__list').getAttribute('hidden')).toBeUndefined;
            expect(el.classList.contains('combobox--autocomplete-open')).toBeTruthy();
        });

        it('should show all listitems if no value is entered', () => {
            const el: HTMLElement = getElement(TPL);
            cb = new Combobox(el);
            cb.data = DATA;

            // Execute
            cb.open();

            let listItems: any = el.querySelectorAll('.combobox__item');
            for (let i: number = 0, j: number = listItems.length; i < j; i++) {
                expect(listItems.item(i).getAttribute('hidden')).toBeUndefined;
                expect((listItems.item(i) as HTMLElement).style.display).toBe('block');
            }
        });

        it('should close the dropdown', () => {
            const el: HTMLElement = getElement(TPL);
            cb = new Combobox(el);
            cb.data = DATA;

            // Execute
            cb.open();
            cb.close();
            jasmine.clock().tick(1);

            // Test
            expect(cb.isOpen).toBeFalsy();
            expect(el.querySelector('.combobox__list').getAttribute('hidden')).toBeDefined;
            expect(el.classList.contains('combobox--autocomplete-open')).toBeFalsy();
        });

        it('should use the given field as label field', () => {
            // Setup
            const el: HTMLElement = getElement(TPL);
            cb = new Combobox(el);
            // noinspection TsLint
            cb['_initTimeout'] = 0;
            cb.inputTimeout = 0;
            cb.labelField = 'label';
            cb.data = DATA;

            // execute
            cb.open();
            jasmine.clock().tick(20);

            expect(el.querySelector('.combobox__item').textContent.trim()).toBe(DATA[0].label);

        });

        it('should apply the labelFunction on each item', () => {
            const el: HTMLElement = getElement(TPL);
            cb = new Combobox(el);
            cb.labelFunction = (e: any) => {
                return 'LABELFUNCTION ' + e.label;
            };
            cb.data = DATA;
            jasmine.clock().tick(2);
            // execute
            cb.open();
            jasmine.clock().tick(20);

            expect(el.querySelector('.combobox__item').textContent.trim()).toBe('LABELFUNCTION ' + DATA[0].label);
        });

        xit('should apply the filtering if the mode is set to autocomplete and the data is changed', (done: any) => {
            const el: HTMLElement = getElement(TPL);
            cb = new Combobox(el);
            // noinspection TsLint
            cb['_initTimeout'] = 0;
            cb.mode = MODE_AUTOCOMPLETE;
            cb.data = [];

            cb.data = DATA_STRINGS;
            expect(cb.isOpen).toBeTruthy();
            done();
        });

        it('should set the value to programmatically', () => {
            const el: HTMLElement = getElement(TPL);
            cb = new Combobox(el);
            cb.labelField = 'label';
            cb.data = DATA;

            // execute
            cb.value = DATA[0];

            expect(el.querySelector('input').value).toBe('Rood');
            expect(cb.value).toEqual({label: 'Rood', value: 'ff0000'});
        });

        it('should throw an error if an invalid item is set programmatically', () => {
            const el: HTMLElement = getElement(TPL);
            cb = new Combobox(el);
            cb.labelField = 'label';
            cb.data = DATA;
            cb.allowUnknown = false;

            let thrown: string;

            // execute
            try {
                cb.value = 'boink';
            } catch (e) {
                thrown = e.message;
            }

            // Test
            expect(thrown).toBe(`Unknown item 'boink'`);
        });

    });

    describe('mouse interaction', () => {

        it('should open the list when the toggle button is clicked and the list is closed', () => {
            const el: HTMLElement = getElement(TPL);
            cb = new Combobox(el);
            cb.data = DATA;

            // execute
            (el.querySelector('.combobox__toggle') as HTMLElement).click();

            expect(cb.isOpen).toBeTruthy();
        });

        it('should close the list when the toggle button is clicked and the list is open', () => {
            const el: HTMLElement = getElement(TPL);
            cb = new Combobox(el);
            cb.data = DATA;
            cb.open();

            // execute
            (el.querySelector('.combobox__toggle') as HTMLElement).click();
            jasmine.clock().tick(1);

            expect(cb.isOpen).toBeFalsy();
        });

        it('should set the value to match the clicked list item', () => {
            const el: HTMLElement = getElement(TPL);
            cb = new Combobox(el);
            // noinspection TsLint
            cb['_initTimeout'] = 0;
            cb.data = DATA_STRINGS;
            cb.open();

            // execute
            jasmine.clock().tick(1);
            (el.querySelector('.combobox__link') as HTMLElement).click();
            expect(el.querySelector('input').value).toBe('rood');
            expect(cb.value).toBe('rood');
        });

        it('should close the list when the user clicks outside the list', () => {
            const el: HTMLElement = getElement(TPL);
            cb = new Combobox(el);
            cb.data = DATA;
            cb.open();
            document.body.click();
            jasmine.clock().tick(1);
            expect(cb.isOpen).toBeFalsy();
        });

        it('should not close the list when the user clicks in the input', () => {
            const el: HTMLElement = getElement(TPL);
            cb = new Combobox(el);
            cb.data = DATA;
            cb.open();
            (el.querySelector('input') as HTMLElement).click();
            expect(cb.isOpen).toBeTruthy();
        });
    });

    describe('keyboard interaction', () => {

        it('should open the dropdown by typing', () => {
            const el: HTMLElement = getElement(TPL);
            cb = new Combobox(el);
            // noinspection TsLint
            cb['_initTimeout'] = 0;
            cb.data = DATA;
            cb.inputTimeout = 0;

            // Execute
            KeyboardEvents.KeyUp(el.querySelector('input'), 'r'.charCodeAt(0));
            jasmine.clock().tick(1);

            // Test
            expect(cb.isOpen).toBeTruthy();
            expect(el.querySelector('.combobox__list').getAttribute('hidden')).toBeUndefined;
            expect(el.classList.contains('combobox--autocomplete-open')).toBeTruthy();
        });

        it('should only be showing items containing the text `ro`', () => {
            const el: HTMLElement = getElement(TPL);
            cb = new Combobox(el);
            // noinspection TsLint
            cb['_initTimeout'] = 0;
            cb.data = DATA_STRINGS;
            cb.inputTimeout = 0;

            // Execute
            // Set the value of the input, we're using a fake event which
            // doesn't actually fill the input
            el.querySelector('input').value = 'ro';
            // Trigger an event to open the dropdown
            KeyboardEvents.KeyUp(el.querySelector('input'), 'r'.charCodeAt(0));
            jasmine.clock().tick(1);
            // Test

            // Only display 'rood', 'groen' & 'roze'
            expect(el.querySelectorAll('.combobox__item:not([hidden])').length).toBe(3);
        });

        it('should only highlight the entered query in the resultlist', () => {
            const el: HTMLElement = getElement(TPL);
            cb = new Combobox(el);
            // noinspection TsLint
            cb['_initTimeout'] = 0;
            cb.data = DATA_STRINGS;
            cb.inputTimeout = 0;

            // Execute
            // Set the value of the input, we're using a fake event which
            // doesn't actually fill the input
            el.querySelector('input').value = 'ro';
            // Trigger an event to open the dropdown
            KeyboardEvents.KeyUp(el.querySelector('input'), 'r'.charCodeAt(0));
            jasmine.clock().tick(1);

            // Test
            // Only display 'rood', 'groen' & 'roze'
            let matches: NodeListOf<Element> = el.querySelectorAll('.combobox__item:not([hidden]) .combobox__link');
            expect(matches.item(0).innerHTML.trim()).toBe('<span class="combobox__match">ro</span>od');
            expect(matches.item(1).innerHTML.trim()).toBe('g<span class="combobox__match">ro</span>en');
            expect(matches.item(2).innerHTML.trim()).toBe('<span class="combobox__match">ro</span>ze');
        });

        it('should close the dropdown using the ESC key', () => {
            const el: HTMLElement = getElement(TPL);
            cb = new Combobox(el);
            // noinspection TsLint
            cb['_initTimeout'] = 0;
            cb.data = DATA;
            cb.inputTimeout = 0;

            // Execute
            cb.open();
            jasmine.clock().tick(1);
            KeyboardEvents.KeyUp(el.querySelector('input'), KEYCODE_ESC);

            // Test
            expect(cb.isOpen).toBeFalsy();
        });

        it('should close the dropdown using the ESC key when focus is on the list', () => {
            const el: HTMLElement = getElement(TPL);
            cb = new Combobox(el);
            // noinspection TsLint
            cb['_initTimeout'] = 0;
            cb.data = DATA;
            cb.inputTimeout = 0;

            // Execute
            cb.open();
            jasmine.clock().tick(1);
            KeyboardEvents.KeyUp(el.querySelector('.combobox__list'), KEYCODE_ESC);

            // Test
            expect(cb.isOpen).toBeFalsy();
        });

        it('should open the dropdown using the ⬇ key', () => {
            const el: HTMLElement = getElement(TPL);
            cb = new Combobox(el);
            // noinspection TsLint
            cb['_initTimeout'] = 0;
            cb.data = DATA;
            cb.inputTimeout = 0;

            // Execute
            KeyboardEvents.KeyUp(el.querySelector('input'), KEYCODE_DOWN);

            // Test
            expect(cb.isOpen).toBeTruthy();
        });

        it('should open the dropdown using the ⬆ key', () => {
            const el: HTMLElement = getElement(TPL);
            cb = new Combobox(el);
            // noinspection TsLint
            cb['_initTimeout'] = 0;
            cb.data = DATA;

            // Execute
            KeyboardEvents.KeyUp(el.querySelector('input'), KEYCODE_UP);

            // Test
            expect(cb.isOpen).toBeTruthy();
        });

        it('should move the focus to the next item of the list using the ⬇ key when the list is already open', () => {
            const el: HTMLElement = getElement(TPL);
            cb = new Combobox(el);
            // noinspection TsLint
            cb['_initTimeout'] = 0;
            cb.inputTimeout = 0;
            cb.data = DATA;
            jasmine.clock().tick(1);

            cb.open();

            jasmine.clock().tick(1);

            const currentItem: HTMLElement = el.querySelector('.combobox__item');
            const nextItem: Element = el.querySelectorAll('.combobox__item').item(1);
            currentItem.focus();

            // Execute
            KeyboardEvents.KeyUp(currentItem, KEYCODE_DOWN);
            jasmine.clock().tick(2);

            // Test
            expect(nextItem).toEqual(document.activeElement);
        });

        it('should move the focus to the previous item of the list using the ⬆ key when the list is already open', () => {
            const el: HTMLElement = getElement(TPL);
            cb = new Combobox(el);
            // noinspection TsLint
            cb['_initTimeout'] = 0;
            cb.inputTimeout = 0;
            cb.data = DATA;
            jasmine.clock().tick(1);
            
            cb.open();

            jasmine.clock().tick(1);

            const previousItem: Element = el.querySelector('.combobox__item');
            const currentItem: HTMLElement = el.querySelectorAll('.combobox__item').item(1) as HTMLElement;
            currentItem.focus();

            // Execute
            KeyboardEvents.KeyUp(currentItem, KEYCODE_UP);
            jasmine.clock().tick(2);

            // Test
            expect(previousItem).toEqual(document.activeElement);
        });

        it('should move the focus to the first item of the list using the ⬇ key', () => {
            const el: HTMLElement = getElement(TPL);
            cb = new Combobox(el);
            // noinspection TsLint
            cb['_initTimeout'] = 0;
            cb.inputTimeout = 0;
            cb.data = DATA;
            jasmine.clock().tick(1);

            cb.open();

            jasmine.clock().tick(1);

            const input: HTMLElement = el.querySelector('.combobox__input');
            const firstItem: Element = el.querySelector('.combobox__item');

            // Execute
            input.focus();
            KeyboardEvents.KeyUp(input, KEYCODE_DOWN);
            jasmine.clock().tick(2);
            
            // Test
            expect(firstItem).toEqual(document.activeElement);
        });

        it('should move the focus to the next item of the list using the ⬇ key', () => {
            const el: HTMLElement = getElement(TPL);
            cb = new Combobox(el);
            // noinspection TsLint
            cb['_initTimeout'] = 0;
            cb.inputTimeout = 0;
            cb.data = DATA;

            jasmine.clock().tick(1);

            cb.open();

            jasmine.clock().tick(1);

            const firstItem: HTMLElement = el.querySelector('.combobox__item');
            const nextItem: Element = el.querySelectorAll('.combobox__item').item(1);
           

            // Execute
            firstItem.focus();
            KeyboardEvents.KeyUp(firstItem, KEYCODE_DOWN);
            jasmine.clock().tick(2);

            // Test
            expect(nextItem).toEqual(document.activeElement);
        });

        it('should move the focus to the previous item of the list using the ⬆ key', () => {
            const el: HTMLElement = getElement(TPL);
            cb = new Combobox(el);
            // noinspection TsLint
            cb['_initTimeout'] = 0;
            cb.data = DATA;
            jasmine.clock().tick(1);

            // Execute
            // First focus on the input
            KeyboardEvents.KeyUp(el.querySelector('input'), KEYCODE_DOWN);
            // Focus is moved to the list
            KeyboardEvents.KeyUp(el.querySelector('.combobox__list'), KEYCODE_DOWN);
            KeyboardEvents.KeyUp(el.querySelector('.combobox__list'), KEYCODE_UP);
            jasmine.clock().tick(2);
            // Test
            expect(el.querySelectorAll('.combobox__item').item(0)).toEqual(document.activeElement);
        });

        it('should set the value of the input to the currently focused item using the ENTER key', () => {
            const el: HTMLElement = getElement(TPL);
            cb = new Combobox(el);
            // noinspection TsLint
            cb['_initTimeout'] = 0;
            cb.data = DATA_STRINGS;
            jasmine.clock().tick(1);
            // Execute
            // First focus on the input
            KeyboardEvents.KeyUp(el.querySelector('input'), KEYCODE_DOWN);
            // Focus is moved to the list
            KeyboardEvents.KeyUp(el.querySelector('.combobox__list'), KEYCODE_ENTER);
            jasmine.clock().tick(2);
            // Test
            expect(el.querySelector('input').value).toEqual('rood');
        });

    });

    describe('events', () => {

        it('should fire a `combobox-select` event when an item is chosen', () => {
            const el: HTMLElement = getElement(TPL);
            const spy: any = createSpy();
            cb = new Combobox(el);
            // noinspection TsLint
            cb['_initTimeout'] = 0;
            cb.data = DATA_STRINGS;
            cb.open();
            jasmine.clock().tick(1);

            el.addEventListener('combobox-select', spy);
            // execute
            jasmine.clock().tick(1);
            (el.querySelector('.combobox__link') as HTMLElement).click();
            expect(spy).toHaveBeenCalled();
        });

        it('should fire a `combobox-open` event when the combobox is opened', () => {
            const el: HTMLElement = getElement(TPL);
            const spy: any = createSpy();
            cb = new Combobox(el);
            // noinspection TsLint
            cb['_initTimeout'] = 0;
            cb.data = DATA_STRINGS;
            el.addEventListener('combobox-open', spy);
            cb.open();

            // execute
            jasmine.clock().tick(1);
            expect(spy).toHaveBeenCalled();
        });

        it('should fire a `combobox-close` event when the combobox is closed', () => {
            const el: HTMLElement = getElement(TPL);
            const spy: any = createSpy();
            cb = new Combobox(el);
            // noinspection TsLint
            cb['_initTimeout'] = 0;
            cb.data = DATA_STRINGS;
            el.addEventListener('combobox-close', spy);
            cb.open();
            jasmine.clock().tick(1);
            cb.close();
            // execute
            jasmine.clock().tick(1);
            expect(spy).toHaveBeenCalled();
        });

        it('should not set the value if a `combobox-select` event is canceled', () => {
            const el: HTMLElement = getElement(TPL);
            cb = new Combobox(el);
            // noinspection TsLint
            cb['_initTimeout'] = 0;
            cb.data = DATA_STRINGS;
            el.addEventListener('combobox-select', (e:CustomEvent) => {
                e.preventDefault();
            });
            cb.open();

            // execute
            jasmine.clock().tick(1);
            (el.querySelector('.combobox__link') as HTMLElement).click();
            expect(el.querySelector('input').value).toEqual('');
        });

        it('should not open the combobox if the `combobox-open` event is canceled', () => {
            const el: HTMLElement = getElement(TPL);
            cb = new Combobox(el);
            // noinspection TsLint
            cb['_initTimeout'] = 0;
            cb.data = DATA_STRINGS;
            el.addEventListener('combobox-open', (e:CustomEvent) => {
                e.preventDefault();
            });
            cb.open();
            jasmine.clock().tick(1);
            expect(cb.isOpen).toBeFalsy();
        });

        it('should not close the combobox if the `combobox-close` event is canceled', () => {
            const el: HTMLElement = getElement(TPL);
            cb = new Combobox(el);
            // noinspection TsLint
            cb['_initTimeout'] = 0;
            cb.data = DATA_STRINGS;
            el.addEventListener('combobox-close', (e:CustomEvent) => {
                e.preventDefault();
            });
            cb.open();
            jasmine.clock().tick(1);
            cb.close();
            // execute
            jasmine.clock().tick(1);
            expect(cb.isOpen).toBeTruthy();
        });
    });

    describe('validation', () => {

        it('should validate the input on input and set it to invalid', () => {
            const el: HTMLElement = getElement(TPL);
            cb = new Combobox(el);
            let input: HTMLInputElement = el.querySelector('input') as HTMLInputElement;
            // noinspection TsLint
            cb['_initTimeout'] = 0;
            cb.data = DATA_STRINGS;
            cb.allowUnknown = false;
            input.value = 'zwart';

            // Execute
            Events.Input(input);

            // Test
            expect(input.checkValidity()).toBeFalsy();
            expect(input.validationMessage).toBe('Ongeldige invoer');
        });

        it('should allow for a custom error message', () => {
            const customError: string = 'Zwart is geen kleur, jeweettoch';
            const el: HTMLElement = getElement(TPL);
            cb = new Combobox(el);
            // noinspection TsLint
            cb['_initTimeout'] = 0;
            let input: HTMLInputElement = el.querySelector('input') as HTMLInputElement;
            cb.validationError = customError;
            cb.data = DATA_STRINGS;
            cb.allowUnknown = false;
            input.value = 'zwart';

            // Execute
            Events.Input(input);

            // Test
            expect(input.checkValidity()).toBeFalsy();
            expect(input.validationMessage).toBe(customError);
        });

        it('should validate the input on input and set it to valid', () => {
            const el: HTMLElement = getElement(TPL);
            cb = new Combobox(el);
            // noinspection TsLint
            cb['_initTimeout'] = 0;
            let input: HTMLInputElement = el.querySelector('input') as HTMLInputElement;
            cb.data = DATA_STRINGS;
            cb.allowUnknown = false;
            input.value = 'rood';

            // Execute
            Events.Input(input);

            // Test
            expect(input.checkValidity()).toBeTruthy();
        });
    });

    describe('teardown', () => {

        it('should remove the toggle listener', () => {
            const el: HTMLElement = getElement(TPL);
            cb = new Combobox(el);

            cb.destroy();

            (el.querySelector('.combobox__toggle') as HTMLElement).click();

            expect(cb.isOpen).toBeFalsy();
        });

        it('should remove the input keyboard listener', () => {
            const el: HTMLElement = getElement(TPL);
            cb = new Combobox(el);

            cb.destroy();

            KeyboardEvents.KeyUp(el.querySelector('input'), KEYCODE_DOWN);

            expect(cb.isOpen).toBeFalsy();
        });

        it('should remove the list element', () => {
            const el: HTMLElement = getElement(TPL);
            cb = new Combobox(el);

            cb.open();
            cb.destroy();
            expect(el.querySelector('.combobox__list')).toBe(null);
        });

        it('should remove the body click listener', () => {
            const el: HTMLElement = getElement(TPL);
            cb = new Combobox(el);
            // noinspection TsLint
            cb['_initTimeout'] = 0;
            cb.data = DATA;
            cb.open();
            cb.destroy();

            document.body.click();

            expect(cb.isOpen).toBeTruthy();
        });
    });

    afterEach(() => {
        jasmine.clock().uninstall();
        if (cb) {
            cb.destroy();
        }
        if (element.parentElement) {
            element.parentNode.removeChild(element);
        }
    });
});
