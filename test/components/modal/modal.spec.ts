import {Modal} from '../../../src/components/modal/modal';
import {Events} from '../../../testutils/Events';
import {} from 'jasmine';
import {KEYCODE_ESC} from '../../../testutils/KeyboardEvents';

describe('Modal component', () => {

    const TEMPLATE: string = `
    <div class="modal">
        <section class="modal__content">
            <header class="modal__title">
                <button class="modal__close-button">
                    <i class="icon icon-cross"></i>
                    Sluit
                </button>
                <h1>Titel van modal</h1>
            </header>

            <div class="modal__body">
                <p>Ut quis congue sapien. Proin maximus augue molestie mauris ultrices, nec sodales ex sollicitudin eget.</p>
            </div>

            <footer class="modal__footer">
                <button class="btn btn--primary">Bevestig</button>
            </footer>
        </section>
    </div>`;

    let element:HTMLElement,
        modal:Modal;

    beforeEach(() => {
        // Add the modal to the dom
        element = document.createElement('div');
        element.innerHTML = TEMPLATE;
        document.body.appendChild(element);
    });

    it('should add the role attribute on initialize', () => {
        // Setup
        let el:HTMLElement = document.querySelector('.modal') as HTMLElement;

        // Execute
        modal = new Modal(el as HTMLElement);

        // Test
        expect(el.getAttribute('role')).toBe('dialog');
    });

    it('should set an ID on the title for aria purposes', () => {
        // Setup
        let el:HTMLElement = document.querySelector('.modal') as HTMLElement;
        let heading:HTMLElement = el.querySelector('h1') as HTMLElement;

        // Execute
        modal = new Modal(el as HTMLElement);

        // Test
        expect(heading.hasAttribute('id')).toBe(true);
    });

    it('should not override the ID of the title if one is set', () => {
        // Setup
        let el:HTMLElement = document.querySelector('.modal') as HTMLElement;
        let heading:HTMLElement = el.querySelector('h1') as HTMLElement;
        heading.setAttribute('id', 'test_id');

        // Execute
        modal = new Modal(el as HTMLElement);

        // Test
        expect(heading.getAttribute('id')).toBe('test_id');
    });

    it('should add the aria labelledby attribute on the dialog', () => {
        // Setup
        let el:HTMLElement = document.querySelector('.modal') as HTMLElement;
        let heading:HTMLElement = el.querySelector('h1') as HTMLElement;
        // Execute
        modal = new Modal(el as HTMLElement);

        // Test
        expect(el.getAttribute('aria-labelledby')).toBe(heading.getAttribute('id'));
    });

    it('should add the modal--open class when the modal opens', () => {
        // Setup
        let el:HTMLElement = document.querySelector('.modal') as HTMLElement;
        modal = new Modal(el as HTMLElement);

        // Execute
        modal.open();

        // Test
        expect(el.classList.contains('modal--open')).toBe(true);
    });

    it('should set the tabindex to prevent focus', () => {
        // Setup
        let el:HTMLElement = document.querySelector('.modal') as HTMLElement;

        // Execute
        modal = new Modal(el as HTMLElement);

        // Test
        expect(el.getAttribute('tabindex')).toBe('-1');
    });

    it('should set the opened state', () => {
        // Setup
        let el:HTMLElement = document.querySelector('.modal') as HTMLElement;
        modal = new Modal(el as HTMLElement);

        // Execute
        modal.open();

        // Test
        expect(el.classList.contains('modal--closed')).toBe(false);
    });

    it('should remove the open state when the close button is clicked', () => {
        // Setup
        let el:HTMLElement = document.querySelector('.modal') as HTMLElement,
            closeBtn:HTMLElement = el.querySelector('.modal__close-button') as HTMLElement;
        modal = new Modal(el);
        modal.open();

        // Execute
        closeBtn.click();

        // Test
        expect(el.classList.contains('modal--open')).toBe(false);
    });

    // ignore test for now, keys not working yet
    it('should close the modal when escape is pressed', ()  => {

        if (window.top['callPhantom']) {
            // Not supported in PhantomJS
            return;
        }

        let el:HTMLElement = document.querySelector('.modal') as HTMLElement;
        modal = new Modal(el);
        modal.open();

        // Execute
        Events.KeyDown(element.querySelector('.modal'), KEYCODE_ESC);

        // Test
        expect(el.classList.contains('modal--open')).toBe(false);

    });

    it('should restore the focus to the previous element when closed', () => {
        // Setup
        let el:HTMLElement = document.querySelector('.modal') as HTMLElement,
            input:HTMLElement = document.createElement('input'),
            closeBtn:HTMLElement = el.querySelector('.modal__close-button') as HTMLElement;

        document.body.appendChild(input);
        input.setAttribute('type', 'text');
        input.focus();

        modal = new Modal(el);
        modal.open();

        // Execute
        closeBtn.click();

        // Test
        expect(document.activeElement).toBe(input);

        // Cleanup
        input.parentElement.removeChild(input);
    });

    it('should set the focus to the first available input inside the modal', () => {
        // Setup
        let el:HTMLElement = document.querySelector('.modal') as HTMLElement,
            input:HTMLElement = document.createElement('input');

        input.setAttribute('type', 'text');
        el.querySelector('.modal__body').appendChild(input);

        // Execute
        modal = new Modal(el);
        modal.open();
        // Test
        expect(document.activeElement).toBe(input);

        // Cleanup
        input.parentElement.removeChild(input);
    });

    // disabled because not working in Phantom and we also want to run on
    xit('should set follow the tabindex not html order when tabbing ', () => {

        if (window.top['callPhantom']) {
            // Not supported in PhantomJS
            return;
        }

        // Setup: add an extra button to test tab order properly
        let el:HTMLElement = document.querySelector('.modal') as HTMLElement;

        // add a bunch of buttons
        el.querySelector('.modal__footer').innerHTML = `
                <button class="btn">Button 1</button>
                <button class="btn">Button 2</button>
                <button class="btn">Button 3</button>
                <button class="btn">Button 4</button>`;

        let nl:NodeList = document.querySelector('.modal').querySelectorAll('button');
        for (let i:number = 0; i < nl.length; i++) {
            (nl.item(i) as HTMLElement).setAttribute('tabindex', (nl.length - i).toString());
        }

        // Execute
        modal = new Modal(el);
        modal.open();
        // set focus to third button in DOM order: Button 3
        (nl.item(3) as HTMLElement).focus();
        // press tab
        Events.KeyDown(element.querySelector('.modal'), 9);

        // Test
        // first check if right item is focussed
        expect(nl.length).toBe(5);
        expect((nl.item(3) as HTMLElement).innerHTML).toBe('Button 3');
        expect((nl.item(3) as HTMLElement).tabIndex).toBe(2);
        // item 3 heeft focus en tabindex 2
        // item 2 heeft tabindex 3 en zou volgende moeten zijn, maar dit werkt niet in PhantomJs
        expect((nl.item(2) as HTMLElement).tabIndex).toBe(3);
        expect(document.activeElement.innerHTML).toBe('Button 2');

    });

    it('should not be possible to focus on element outside of modal', () => {

        if (window.top['callPhantom']) {
            // Not supported in PhantomJS
            return;
        }

        // Setup
        let el:HTMLElement = document.querySelector('.modal') as HTMLElement;
        el.insertAdjacentHTML('beforebegin', '<a href="#">before</a>');
        el.insertAdjacentHTML('afterend', '<a href="#">before</a>');
        // Execute
        modal = new Modal(el);
        modal.open();

        Events.KeyDown(element.querySelector('.modal'), 9);

        // Test
        expect(document.activeElement.classList.contains('modal__close-button')).toBe(true);
    });

    it('should set the focus to the primary button if no input is placed inside the modal', () => {
        // Setup
        let el:HTMLElement = document.querySelector('.modal') as HTMLElement;
        el.querySelector('.modal__footer').innerHTML = `
                <button class="btn">Button 1</button>
                <button class="btn">Button 2</button>
                <button class="btn">Button 3</button>
                <button class="btn btn--primary">Button 4</button>`;

        // Execute
        modal = new Modal(el);
        modal.open();
        // Test
        expect(document.activeElement.innerHTML).toBe('Button 4');
    });

    it('should set the focus to the any button if no input is placed inside the modal and no primary button is present', () => {
        // Setup
        let el:HTMLElement = document.querySelector('.modal') as HTMLElement;
        el.querySelector('.modal__footer').innerHTML = `
                <button class="btn">Button 1</button>
                <button class="btn">Button 2</button>
                <button class="btn">Button 3</button>
                <button class="btn">Button 4</button>`;
        // Execute
        modal = new Modal(el);
        modal.open();
        // Test
        expect(document.activeElement.innerHTML).toBe('Button 1');
    });

    it('should set the focus to the close button if no input or buttons are placed inside the modal', () => {
        // Setup
        let el:HTMLElement = document.querySelector('.modal') as HTMLElement,
            closeBtn:HTMLElement = el.querySelector('.modal__close-button') as HTMLElement;
        el.querySelector('.modal__footer').innerHTML = '';
        // Execute
        modal = new Modal(el);
        modal.open();
        // Test
        expect(document.activeElement).toBe(closeBtn);
    });

    it('should open a modal without error even if no close button exists on modal', () => {
        // Setup
        let el:HTMLElement = document.querySelector('.modal') as HTMLElement,
          closeBtn:HTMLElement = el.querySelector('.modal__close-button') as HTMLElement;
        closeBtn.parentNode.removeChild(closeBtn);

        // Execute
        modal = new Modal(el);
        modal.open();

        // Test
        expect(el.classList.contains('modal--open')).toBe(true);
    });

    afterEach(() => {
        if (modal) {
            modal.destroy();
        }
        element.parentElement.removeChild(element);
    });
});
