import {Notification} from '../../../src/components/notification/notification';
import {} from 'jasmine';

describe('Notification component', () => {
    const TEMPLATE: string = `
    <div class="notification notification--info">
        <div class="notification__content">
            <div class="notification__type">TER INFORMATIE</div>
            <strong class="notification__title">Voor de veiligheid</strong>
            <span>Vanwege de veiligheid sturen we u het wachtwoord en tokengenerator in 2 aparte enveloppen.</span>
        </div>
        <button class="notification__close-button" onclick="notification.close()">
            <i class="icon icon-cross"></i>
            Sluit
        </button>
    </div>`;

    let element:HTMLElement,
        notification:Notification;

    beforeEach(() => {
        // Add the notification to the dom
        element = document.createElement('div');
        element.innerHTML = TEMPLATE;
        document.body.appendChild(element);
    });

    it('should remove the notification element when the close function is called', ()  => {
        let el:HTMLElement = document.querySelector('.notification') as HTMLElement;
        notification = new Notification(el);

        // Execute
        notification.close();

        // Test
        expect(document).not.toContain(el);

    });

    afterEach(() => {
        element.parentElement.removeChild(element);
    });
});