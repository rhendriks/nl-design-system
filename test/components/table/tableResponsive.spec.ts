import {} from 'jasmine';
import {TableResponsive} from '../../../src/components/table/TableResponsive';

describe('Table responsive component', () => {

    const TEMPLATE: string = `<table class="table table--striped">
                        <thead>
                        <tr>
                            <td></td>
                            <th>2014</th>
                            <th>2015</th>
                            <th>2016</th>
                        </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <th>Foo</th>
                                <td>124</td>
                                <td>42</td>
                                <td>252</td>
                            </tr>
                            <tr>
                                <th>Bar</th>
                                <td>42</td>
                                <td>526</td>
                                <td>12</td>
                            </tr>
                            <tr>
                                <th>Baz</th>
                                <td>736</td>
                                <td>367</td>
                                <td>234</td>
                            </tr>
                        </tbody>
                    </table>`;

    let element:HTMLElement,
        table:HTMLElement;

    beforeEach(() => {
        // Add the modal to the dom
        element = document.createElement('div');
        element.innerHTML = TEMPLATE;
        document.body.appendChild(element);
        table = document.querySelector('table') as HTMLElement
    });

    it('should add the responsive class', () => {
        // Setup
        // Execute
        new TableResponsive(table);

        // Test
        expect(table.classList.contains('table--responsive')).toBe(true);
    });

    it('should add data-col="2014" to the first data column', () => {
        // Setup
        // Execute
        new TableResponsive(table);
        const rows:NodeList = table.querySelectorAll('tbody tr');

        // Test
        for (let i:number = 0; i < rows.length; i++) {
            const cell:Element = (rows.item(i) as Element).querySelector('td:nth-of-type(1)');
            expect(cell.getAttribute('data-col')).toBe('2014');
        }

    });

    it('should add data-col="2015" to the second data column', () => {
        // Setup
        // Execute
        new TableResponsive(table);
        const rows:NodeList = table.querySelectorAll('tbody tr');

        // Test
        for (let i:number = 0; i < rows.length; i++) {
            const cell:Element = (rows.item(i) as Element).querySelector('td:nth-of-type(2)');
            expect(cell.getAttribute('data-col')).toBe('2015');
        }

    });

    it('should add data-col="2016" to the third data column', () => {
        // Setup
        // Execute
        new TableResponsive(table);
        const rows:NodeList = table.querySelectorAll('tbody tr');

        // Test
        for (let i:number = 0; i < rows.length; i++) {
            const cell:Element = (rows.item(i) as Element).querySelector('td:nth-of-type(3)');
            expect(cell.getAttribute('data-col')).toBe('2016');
        }

    });

    afterEach(() => {
        element.parentElement.removeChild(element);
    });
});
