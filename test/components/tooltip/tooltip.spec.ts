import {} from 'jasmine';
import {Tooltip} from '../../../src/components/tooltip/tooltip';
import {Events} from '../../../testutils/Events';

describe('Tooltip component', () => {

    const TOOLTIP_CONTENT: string = 'Tooltip title';

    let holder: HTMLElement,
        host: HTMLElement,
        tooltip:Tooltip;

    beforeEach(() => {
        holder = document.createElement('div');
        holder.classList.add('.test-holder');

        host = document.createElement('dfn');
        host.innerHTML = 'trigger';
        host.setAttribute('title', TOOLTIP_CONTENT);

        holder.appendChild(host);
        document.body.appendChild(holder);
    });

    it('should check for a title attribute', () => {
        let el: HTMLElement = document.createElement('div'),
            thrown: boolean = false,
            message: string = 'tooltip is missing a title attribute';

        try {
            tooltip = new Tooltip(el);

        } catch (e) {
            if (e.message === message) {
                thrown = true;
            }
        }

        expect(thrown).toBe(true);

    });

    it('should add the class `.panel-trigger` to the host component', () => {
        // Setup
        // Execute
        tooltip = new Tooltip(host);

        // Test
        expect(host.classList.contains('panel-trigger')).toBe(true);

    });

    it('should remove the title attribute', () => {
        // Setup
        // Execute
        tooltip = new Tooltip(host);

        // Test
        expect(host.getAttribute('title')).toBe(null);

    });

    it('should add a stylesheet to the document head', () => {
        // Setup
        // Execute
        tooltip = new Tooltip(host);

        // Test
        expect(document.querySelectorAll('style[x-uno-panel-stylesheet]').length).toBe(1);

    });

    it('should not add a 2nd stylesheet to the document head', () => {
        // Setup
        let clone: HTMLElement = host.cloneNode() as HTMLElement,
            clonedTooltip:Tooltip;
        holder.appendChild(clone);
        // Execute
        tooltip = new Tooltip(host);

        clonedTooltip = new Tooltip(clone);

        // Test
        expect(document.querySelectorAll('style[x-uno-panel-stylesheet]').length).toBe(1);

        clonedTooltip.destroy();

    });

    it('should set the content of the panel on mouse over', () => {
        // Setup
        document.body.appendChild(holder);
        tooltip = new Tooltip(host);

        // Execute
        Events.MouseOver(host);

        expect(tooltip.panelBody.innerHTML).toBe(TOOLTIP_CONTENT);
    });

    afterEach(() => {
        let querySelectors: Array<string> = [
            '.test-holder',
            '.tooltip'
        ];

        if (tooltip) {
            tooltip.destroy();
        }

        let elements: NodeList = document.querySelectorAll(querySelectors.join(','));

        for (let i: number = 0; i < elements.length; i++) {
            let el: Element = elements.item(i) as Element;
            if (el && el.parentElement) {
                el.parentElement.removeChild(el);
            }
        }
    });

    afterAll(() => {
        let querySelectors: Array<string> = [
            '.test-holder',
            '.tooltip',
            'style[x-uno-tooltip-stylesheet]'
        ];

        let elements: NodeList = document.querySelectorAll(querySelectors.join(','));

        for (let i: number = 0; i < elements.length; i++) {
            let el: Element = elements.item(i) as Element;
            if (el && el.parentElement) {
                el.parentElement.removeChild(el);
            }
        }

    })

});