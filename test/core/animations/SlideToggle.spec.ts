import {SlideToggle} from '../../../src/core/animations/SlideToggle';
import {} from 'jasmine';

describe('SlideToggle animation', () => {

    const TEMPLATE: string = `<div>
    <style>
        .testClass {
            display: none;
            /* Set line-height so we'll know what the height will become */
            line-height: 12px;
        }

        .testClass.animate--open {
            display: block;
        }
</style>
<div class="testClass">
    Content of testclass
</div>
</div>`;

    function getTemplate(): Element {
        let el: HTMLElement = document.createElement('div');
        el.innerHTML = TEMPLATE;
        return el.firstElementChild;
    }

    beforeAll(() => {
        // Mock requestAnimationFrame because Firefox won't support it in tests
        // it skips too many frames
        spyOn(window, 'requestAnimationFrame').and.callFake((cb:any) => {
            cb();
        });
    });

    xit('should check if the browser supports animation', () => {
        document.body.appendChild(getTemplate());
        let testClass: HTMLElement = document.querySelector('.testClass') as HTMLElement,
            st: SlideToggle = new SlideToggle(testClass);

    });

    // Not sure if this is a safe way to test
    it('should dispatch the `uno-animation-start` event when the animation is started', (done: Function) => {
        // Setup
        document.body.appendChild(getTemplate());
        let testClass: HTMLElement = document.querySelector('.testClass') as HTMLElement,
            st: SlideToggle = new SlideToggle(testClass);

        spyOn(testClass, 'dispatchEvent').and.callFake(
            (evt: CustomEvent) => {
                if (evt.type === `uno-animation-start`) {
                    done();
                }
            });

        // Execute
        st.start();
    });

    // Not sure if this is a safe way to test
    it('should dispatch the `uno-animation-progress` event for each animation iteration', (done: Function) => {
        // Setup
        document.body.appendChild(getTemplate());
        let testClass: HTMLElement = document.querySelector('.testClass') as HTMLElement,
            st: SlideToggle = new SlideToggle(testClass),
            callCount: number = 0;

        st.animationDuration = 20;

        spyOn(testClass, 'dispatchEvent').and.callFake(
            (evt: CustomEvent) => {
                if (evt.type === `uno-animation-progress`) {
                    callCount++;
                }
            });
        // Execute
        st.start();

        // Test
        setTimeout(() =>
            {
                expect(callCount).toBe(21);
                done();
            },
                   200);

    });

    it('should dispatch the `uno-animation-end` event when the animation is finished', (done: any) => {
        // Setup
        document.body.appendChild(getTemplate());
        let testClass: HTMLElement = document.querySelector('.testClass') as HTMLElement,
            st: SlideToggle = new SlideToggle(testClass);

        st.animationDuration = 20;

        let called:boolean = false;
        spyOn(testClass, 'dispatchEvent').and.callFake(
            (evt: CustomEvent) => {
                if (evt.type === `uno-animation-end`) {
                    called = true;
                }
            });
        // Execute
        st.start();

        // Test
        setTimeout(() =>
            {
                expect(called).toBeTruthy();
                done();
            },
                   200);

    });

    it('should throw an error if an unknown ease function is set', () => {
        // Setup
        document.body.appendChild(getTemplate());
        let testClass: HTMLElement = document.querySelector('.testClass') as HTMLElement,
            st: SlideToggle = new SlideToggle(testClass),
            err: string = '';

        // Execute
        try {
            st.ease = 'non-existing-ease';
        } catch (e) {
            err = e.message;
        }

        // Test
        expect(err).toBe("Easing equation 'non-existing-ease' does not exist");

    });

    afterEach(() => {
        let testClasses: NodeList = document.querySelectorAll('.testClass');
        for (let i: number = 0; i < testClasses.length; i++) {
            testClasses.item(i).parentElement.removeChild(testClasses.item(i));
        }
    });
});
