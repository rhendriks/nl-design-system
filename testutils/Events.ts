import {KeyboardEvents} from './KeyboardEvents';

export class Events {

    public static KeyDown(element:any, code:any):void {
        KeyboardEvents.KeyDown(element, code);
    }

    public static TouchStart(element:any):void {
        Events.dispatch(element, 'TouchEvent', 'touchstart', [0, 0, 0, 0, 0, false, false, false, false, 0, null]);
    }

    public static MouseOver(element:any):void {
        Events.dispatch(element, 'MouseEvent', 'mouseover', [0, 0, 0, 0, 0, false, false, false, false, 0, null]);
    }

    public static Input(element:any):void {
        Events.dispatch(element, 'Event', 'input', []);
    }

    private static dispatch(element:HTMLElement, eventType:string, type:string, params:Array<any>):void {
        let event:any = document.createEvent(eventType),
            eventParams:Array<any> = [type, true, true, document.defaultView, ...params];

        switch (eventType) {

            case 'MouseEvent':
                event.initMouseEvent(...eventParams);
                break;
            default:
                event.initEvent(...eventParams);
        }

        element.dispatchEvent(event);
    }

}
